package
{
		
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.engine.ElementFormat;
	import flash.text.engine.FontDescription;
	import flash.text.engine.FontLookup;
	
	import feathers.controls.Button;
	import feathers.controls.ButtonState;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollText;
	import feathers.controls.renderers.BaseDefaultItemRenderer;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.text.StageTextTextEditor;
	import feathers.controls.text.TextBlockTextEditor;
	import feathers.controls.text.TextBlockTextRenderer;
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.core.FeathersControl;
	import feathers.core.ITextEditor;
	import feathers.core.ITextRenderer;
	import feathers.themes.StyleNameFunctionTheme;
	
	import starling.display.Quad;
	
	public class CustomTheme extends StyleNameFunctionTheme
	{
		[Embed(source="assets/font.ttf", fontFamily="EmojiFont", fontWeight="normal", fontStyle="normal", mimeType="application/x-font", embedAsCFF="true")]
		private static const MY_FONT:Class;	
		
		
		public function CustomTheme()
		{
			super();
			
			this.initialize();
		}
		
		private function initialize():void
		{
			this.initializeGlobals();
			this.initializeStyleProviders();	
		}
		
		private function initializeGlobals():void
		{
			FeathersControl.defaultTextRendererFactory = function():ITextRenderer
			{
				return new TextBlockTextRenderer();
			}
			
			FeathersControl.defaultTextEditorFactory = function():ITextEditor
			{
				return new StageTextTextEditor();
			}
		}
		
		private function initializeStyleProviders():void
		{
			
			this.getStyleProviderForClass(Button).setFunctionForStyleName("back-button", setBackButtonStyles);
			
			this.getStyleProviderForClass(Label).setFunctionForStyleName("custom-label", setCustomLabelStyles);
			
			this.getStyleProviderForClass(DefaultListItemRenderer).defaultStyleFunction = this.setDefaultListItemRendererStyles;
			this.getStyleProviderForClass(Header).defaultStyleFunction = this.setHeaderStyles;
			this.getStyleProviderForClass(PanelScreen).defaultStyleFunction = this.setPanelScreenStyles;
			this.getStyleProviderForClass(ScrollText).defaultStyleFunction = this.setScrollTextStyles;
			
			//this.getStyleProviderForClass(TextInput).defaultStyleFunction = this.setTextInputStyles;
		}
		
		//-------------------------
		// Button
		//-------------------------
				
		private function setBackButtonStyles(button:Button):void
		{
			var transparentQuad:Quad = new Quad(3, 3, 0xFFFFFF);
			transparentQuad.alpha = 0.20;
			
			var arrowIcon:ImageLoader = new ImageLoader();
			arrowIcon.width = arrowIcon.height = 25;
			arrowIcon.source = "assets/icons/back-arrow.png";
			
			button.width = button.height = 45;
			button.defaultIcon = arrowIcon;
			button.downSkin = transparentQuad;
		}
				
		//-------------------------
		// Header
		//-------------------------
		
		private function setHeaderStyles(header:Header):void
		{
			var quad:Quad = new Quad(3, 50);			
			quad.setVertexColor(0, 0xADBF69);
			quad.setVertexColor(1, 0xADBF69);
			quad.setVertexColor(2, 0x8BA629);
			quad.setVertexColor(3, 0x8BA629);
			
			header.backgroundSkin = quad;
			
			header.titleFactory = function():ITextRenderer
			{
				var renderer:TextBlockTextRenderer = new TextBlockTextRenderer();
				renderer.elementFormat = new ElementFormat(new FontDescription("_sans"), 16, 0xFFFFFF);
				return renderer;
			}		
		}
		
		//-------------------------
		// List
		//-------------------------
				
		private function setDefaultListItemRendererStyles(renderer:DefaultListItemRenderer):void
		{
			renderer.defaultSkin = new Quad(3, 3, 0xFFFFFF);
			renderer.downSkin = new Quad(3, 3, 0x8BA629);
			renderer.defaultSelectedSkin = new Quad(3, 3, 0x8BA629);
			
			renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			renderer.paddingLeft = 10;
			renderer.paddingRight = 10;
			renderer.paddingTop = 10;
			renderer.paddingBottom = 10;
			renderer.minHeight = 50;
			renderer.gap = 10;
			
			renderer.labelFactory = function():ITextRenderer
			{
				var blackFormat:TextFormat = new TextFormat("_sans", 14, 0x000000);
				blackFormat.leading = 7;
				blackFormat.align = TextFormatAlign.LEFT;
				
				var whiteFormat:TextFormat = new TextFormat("_sans", 14, 0xFFFFFF);
				whiteFormat.leading = 7;
				whiteFormat.align = TextFormatAlign.LEFT;
				
				var renderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				renderer.wordWrap = true;
				renderer.isHTML = true;
				
				renderer.setTextFormatForState(ButtonState.UP, blackFormat);
				renderer.setTextFormatForState(ButtonState.UP_AND_SELECTED, whiteFormat);
				renderer.setTextFormatForState(ButtonState.DOWN, whiteFormat);
				renderer.setTextFormatForState(ButtonState.DOWN_AND_SELECTED, whiteFormat);
				renderer.setTextFormatForState(ButtonState.HOVER, blackFormat);
				renderer.setTextFormatForState(ButtonState.HOVER_AND_SELECTED, whiteFormat);

				
				return renderer;				
				
			}
		}		
		
		//-------------------------
		// PanelScreen
		//-------------------------
		
		private function setPanelScreenStyles(screen:PanelScreen):void
		{
			screen.backgroundSkin = new Quad(3, 3, 0xFFFFFF);
		}
		
		private function setCustomLabelStyles(label:Label):void
		{
			label.textRendererFactory = function():ITextRenderer
			{
				var renderer:TextBlockTextRenderer = new TextBlockTextRenderer();
				
				var font:FontDescription = new FontDescription("EmojiFont");
				font.fontLookup = FontLookup.EMBEDDED_CFF;
				
				renderer.elementFormat = new ElementFormat(font, 16, 0xFFFFFF);
				
				
				//var renderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				//renderer.textFormat = new TextFormat("EmojiFont", 24);
				return renderer;
			}
		}
		
		//-------------------------
		// ScrollText
		//-------------------------
				
		private function setScrollTextStyles(scrollText:ScrollText):void
		{
			var textFormat:TextFormat =  new TextFormat("_sans", 14, 0x000000);
			textFormat.leading = 7;
			
			scrollText.textFormat = textFormat;
			scrollText.isHTML = true;
		}
		
	}		
}