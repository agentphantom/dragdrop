package
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.ScrollContainer;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.controls.renderers.LayoutGroupListItemRenderer;
	import feathers.dragDrop.DragData;
	import feathers.dragDrop.DragDropManager;
	import feathers.dragDrop.IDragSource;
	import feathers.dragDrop.IDropTarget;
	import feathers.events.DragDropEvent;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.utils.touch.LongPress;
	
	import starling.display.DisplayObject;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	
	public class DragableRenderer extends LayoutGroupListItemRenderer implements IListItemRenderer, IDragSource, IDropTarget
	{
		private const LIST_ITEM_DRAG:String = "LIST_ITEM_DRAG";
		protected var _label:Label;
		protected var _subtitle:Label;
		protected var _accessorylabel:Label;
		protected var _hiliteTop:Quad;
		protected var _hiliteBottom:Quad;
		protected var _dragHandle:Quad;
		protected var _dragHandleButton:Button;
		protected var _backgroundQuad:Quad;
		
		protected var _longPress:LongPress;
		
		public function DragableRenderer()
		{
			super();
			
			this._longPress = new LongPress(this);
			
			//drag
			this.addEventListener(DragDropEvent.DRAG_START, dragStartHandler);
			this.addEventListener(DragDropEvent.DRAG_COMPLETE, dragCompleteHandler);
			
			//drop
			this.addEventListener(DragDropEvent.DRAG_ENTER, dragEnterHandler);
			this.addEventListener(DragDropEvent.DRAG_EXIT, dragExitHandler);
			this.addEventListener(DragDropEvent.DRAG_MOVE, dragMoveHandler);            
			this.addEventListener(DragDropEvent.DRAG_DROP, dragDropHandler);
			
		}
		
		override protected function initialize():void
		{
			this.layout = new AnchorLayout();
			this.minHeight = 50;
			
			this._label = new Label();			
			this._label.layoutData = new AnchorLayoutData(5, 50, NaN, 10, NaN, NaN);			
			this._label.height = 20;
			this.addChild(this._label);
			
			this._subtitle = new Label();
			this._subtitle.layoutData = new AnchorLayoutData(NaN, 50, 5, 10, NaN, NaN);
			this._subtitle.height = 20;
			this.addChild(this._subtitle);
			
			this._accessorylabel = new Label();
			this._accessorylabel.layoutData = new AnchorLayoutData(NaN, 10, NaN, NaN, NaN, 0);
			this._accessorylabel.height = 20;
			this.addChild(this._accessorylabel);
			
			this._hiliteTop = new Quad(3, 3, 0x000000);
			this._hiliteTop.visible = false;
			this.addChild(_hiliteTop);
			
			this._hiliteBottom = new Quad(3, 3, 0x000000);
			this._hiliteBottom.visible = false;         
			this.addChild(_hiliteBottom);
			
			this._dragHandleButton = new Button();
			this._dragHandleButton.alpha = 0;
			this._dragHandleButton.defaultSkin = new Quad(3, 3, 0x0099FF);
			this._dragHandleButton.width = this._dragHandleButton.height = 35;
			this._dragHandleButton.layoutData = new AnchorLayoutData(0, 0, 0, 0, NaN, NaN);
			this._dragHandleButton.isLongPressEnabled = true;			
			
			this.addChild(_dragHandleButton);
			
			this._backgroundQuad = new Quad(3, 3, 0xFFFFFF);
			this.backgroundSkin = this._backgroundQuad;						
			
			this.addEventListener(FeathersEventType.LONG_PRESS, longPressStarted);
			
		}
		
		
		protected function longPressStarted(event:Event):void
		{
			var dragStartedEvent:Event = new Event("drag-started", true);
			dispatchEvent(dragStartedEvent);
			
			//LongPress started
			
			this._backgroundQuad.color = 0x0099FF;			
			
			//Avoid the list and the Main Scroller to scroll
			Main.mainGroup.stopScrolling();
			this._owner.stopScrolling();
			
			//We start creating the Avatar
			
			var avatarBG:Quad = new Quad(this.width, this.height, 0x0099FF);
			avatarBG.alpha = 0.25;
			
			avatarContainer = new LayoutGroup();
			avatarContainer.layout = new AnchorLayout();					
			avatarContainer.backgroundSkin = avatarBG;
			
			avatarlabel1 = new Label();			
			avatarlabel1.layoutData = new AnchorLayoutData(5, 50, NaN, 10, NaN, NaN);			
			avatarlabel1.height = 20;
			avatarlabel1.text = this._label.text;
			avatarContainer.addChild(avatarlabel1);
			
			avatarlabel2 = new Label();
			avatarlabel2.layoutData = new AnchorLayoutData(NaN, 50, 5, 10, NaN, NaN);
			avatarlabel2.height = 20;
			avatarlabel2.text = this._subtitle.text;
			avatarContainer.addChild(avatarlabel2);
			
			avatarlabel3 = new Label();
			avatarlabel3.layoutData = new AnchorLayoutData(NaN, 10, NaN, NaN, NaN, 0);
			avatarlabel3.height = 20;
			avatarlabel3.text = this._accessorylabel.text;
			avatarContainer.addChild(this._accessorylabel);
			
			var dragData:DragData = new DragData();
			dragData.setDataForFormat(LIST_ITEM_DRAG, this._data);
			DragDropManager.startDrag(this, new Touch(0), dragData, avatarContainer, this.width/2*-1, this.height/2*-1);
			
			event.stopPropagation();
			
			//We start to listen for a TouchEvent
			
			this.addEventListener(TouchEvent.TOUCH, enterframeHandler);
			
		}
		
		private function enterframeHandler(event:TouchEvent):void
		{
			
			//Depending where the mouse/finger is located we scroll the list
			
			if(DragDropManager.isDragging)
			{
				
				//check if the drag is close enough to the top / bottom to start scrolling
				var touchScrollTest:Touch = event.getTouch(this);
				
				if(touchScrollTest)
				{   
					if(touchScrollTest.globalY < this.owner.y)
					{
						startScrollingList(SCROLL_DIRECTION_DOWN, SCROLL_SPEED_FAST);       
					}
					else if(touchScrollTest.globalY < this.owner.y + LIST_SCROLL_DETECT_HEIGHT)
					{                       
						startScrollingList(SCROLL_DIRECTION_DOWN, SCROLL_SPEED_SLOW);
					}
					else if(touchScrollTest.globalY > this.owner.y + this.owner.height)
					{
						startScrollingList(SCROLL_DIRECTION_UP, SCROLL_SPEED_FAST);
					}
					else if(touchScrollTest.globalY > this.owner.y + this.owner.height - LIST_SCROLL_DETECT_HEIGHT)
					{                       
						startScrollingList(SCROLL_DIRECTION_UP, SCROLL_SPEED_SLOW);
					}
					
					//Left and right are hardcoded since they don't belong to the list but the ScrollContaier that holds the list
					
					else if(touchScrollTest.globalX < this.owner.width/2-100)
					{                       
						startScrollingList(SCROLL_DIRECTION_LEFT, SCROLL_SPEED_SLOW);
					}					
					else if(touchScrollTest.globalX > this.owner.width/2+100)
					{                       
						startScrollingList(SCROLL_DIRECTION_RIGHT, SCROLL_SPEED_SLOW);
					}					
					else
					{   
						stopScrolling();        
					}
				}
				
				return;
			}
		}
		
		
		override protected function commitData():void
		{
			if(this._data && this._owner)
			{
				_label.text = _data.label;
				_subtitle.text = _data.sublabel;
				_accessorylabel.text = _data.accessorylabel;
				_backgroundQuad.color = 0xFFFFFF;
			} else{
				_label.text = "";
				_subtitle.text = "";
				_accessorylabel.text = "";
			}
		}
		
		override protected function preLayout():void
		{
			this._hiliteTop.width = this.width;
			this._hiliteBottom.width = this.width;
		}
		
		//Drag Code
		
		private function dragStartHandler(event:DragDropEvent, dragData:DragData):void
		{
			Main.originalOwner = this.owner;
			
			this._backgroundQuad.color = 0x0099FF;
			
			//the drag was started with the call to DragDropManager.startDrag()         
		}
		
		private function dragCompleteHandler(event:DragDropEvent, dragData:DragData):void
		{
			if(event.isDropped)
			{
				this._backgroundQuad.color = 0xFFFFFF;
				
				//the object successfully dropped at a valid location
			}                       
			stopScrolling();
		}
		
		//Variables and constants for the scrolling while dragging functionality
		
		private var carryOnScrolling:Boolean=false;
		private var scrollDirection:String="";
		private var scrollSpeed:String="";
		private var scrollTimer:Timer;
		private var avatarContainer:LayoutGroup;
		private var avatarlabel1:Label;
		private var avatarlabel2:Label;
		private var avatarlabel3:Label;		
		
		private const LIST_SCROLL_DETECT_HEIGHT:int = 30;		
		private const SCROLL_SIZE:int = 25;
		
		private const SCROLL_SPEED_SLOW:String = "SLOW";
		private const SCROLL_SPEED_FAST:String = "FAST";
		
		private const SCROLL_DIRECTION_UP:String = "UP";
		private const SCROLL_DIRECTION_DOWN:String = "DOWN";
		
		private const SCROLL_DIRECTION_LEFT:String = "LEFT";
		private const SCROLL_DIRECTION_RIGHT:String = "RIGHT";
		
		private const SCROLL_TIME_MS_SLOW:int = 75;
		private const SCROLL_TIME_MS_FAST:int = 25;
		private var scrollSpeedMSToUse:int;
		
		private function startScrollingList(direction:String, speed:String):void
		{
			scrollDirection = direction;
			scrollSpeed = speed;
			carryOnScrolling = true;
			
			doScrollWorker();
			
			if(scrollTimer != null)
			{
				scrollTimer.stop();
				scrollTimer = null;
			}
			
			
			if(scrollSpeed == SCROLL_SPEED_FAST)
				scrollSpeedMSToUse = SCROLL_TIME_MS_FAST;
			else
				scrollSpeedMSToUse = SCROLL_TIME_MS_SLOW;
			
			scrollTimer = new Timer(scrollSpeedMSToUse);
			scrollTimer.addEventListener(TimerEvent.TIMER, onScrollTimerHandler);
			scrollTimer.start();
		}
		
		private function stopScrolling():void
		{           
			carryOnScrolling = false;
		}
		
		private function onScrollTimerHandler(e:TimerEvent):void
		{   
			if(carryOnScrolling)
			{
				doScrollWorker();
			}
			else
			{
				scrollTimer.stop();
				scrollTimer = null;
			}
		}
		
		private function doScrollWorker():void
		{
			const EXTRA_SCROLL_PADDING:int = 20;
			
			var scrollSizeToUse:int = SCROLL_SIZE;
			var mainGroup:ScrollContainer = this.owner.parent.parent as ScrollContainer; //We reference the ScrollContaier that holds the list

			
			if(scrollDirection == SCROLL_DIRECTION_UP)
			{   
				if((this.owner.verticalScrollPosition + scrollSizeToUse) < (this.owner.maxVerticalScrollPosition + EXTRA_SCROLL_PADDING))
				{
					this.owner.scrollToPosition(this.owner.horizontalScrollPosition, this.owner.verticalScrollPosition + scrollSizeToUse, (scrollSpeedMSToUse / 1000));
				}   
			}
			else if(scrollDirection == SCROLL_DIRECTION_DOWN)
			{
				if((this.owner.verticalScrollPosition - scrollSizeToUse) > (this.owner.minVerticalScrollPosition - EXTRA_SCROLL_PADDING))
				{
					this.owner.scrollToPosition(this.owner.horizontalScrollPosition, this.owner.verticalScrollPosition - scrollSizeToUse, (scrollSpeedMSToUse / 1000));
				}   
			}			
			else if(scrollDirection == SCROLL_DIRECTION_DOWN)
			{
				if((this.owner.verticalScrollPosition - scrollSizeToUse) > (this.owner.minVerticalScrollPosition - EXTRA_SCROLL_PADDING))
				{
					this.owner.scrollToPosition(this.owner.horizontalScrollPosition, this.owner.verticalScrollPosition - scrollSizeToUse, (scrollSpeedMSToUse / 1000));
				}   
			}
			else if(scrollDirection == SCROLL_DIRECTION_LEFT)
			{				
				mainGroup.scrollToPosition(mainGroup.horizontalScrollPosition - 30, mainGroup.verticalScrollPosition, 0.3)
			}
			else if(scrollDirection == SCROLL_DIRECTION_RIGHT)
			{				
				mainGroup.scrollToPosition(mainGroup.horizontalScrollPosition + 30, mainGroup.verticalScrollPosition, 0.3)
			} else {
				
			}
		}
		
		
		private var _background:Quad;
		private var _touchID:int = -1;
		private var _draggedObject:DisplayObject;
		
		//Drop Code
		
		private function dragEnterHandler(event:DragDropEvent, dragData:DragData):void
		{
			if(!dragData.hasDataForFormat(LIST_ITEM_DRAG))
			{
				return;
			}
			DragDropManager.acceptDrag(this);
		}
		
		private function dragMoveHandler(event:DragDropEvent, dragData:DragData):void
		{
			var dataBeingDragged:Object = dragData.getDataForFormat(LIST_ITEM_DRAG);            
			if(dataBeingDragged != this._data)
			{
				if(event.localY < (this.height / 2))
				{
					this._hiliteTop.visible = true;
					this._hiliteBottom.visible = false;
				}
				else
				{
					this._hiliteTop.visible = false;
					this._hiliteBottom.visible = true;
				}               
			}
		}
		
		private function dragExitHandler(event:DragDropEvent, dragData:DragData):void
		{
			this._label.text = this._data.label;
			this._hiliteTop.visible = false;
			this._hiliteBottom.visible = false;
		}
		
		private function dragDropHandler(event:DragDropEvent, dragData:DragData):void
		{		
			var dragStoppedEvent:Event = new Event("drag-stopped", true);
			dispatchEvent(dragStoppedEvent);
			
			Main.currentOwner = this.owner;
			
			var dataBeingDragged:Object = dragData.getDataForFormat(LIST_ITEM_DRAG);            
			if(dataBeingDragged != this._data)      
			{
				//We set the original owner so the data doesnt get duplicated
				Main.originalOwner.dataProvider.removeItem(dataBeingDragged);
				
				var indexToDragTo:int = this.owner.dataProvider.getItemIndex(this.data);
				
				if(this._hiliteBottom.visible)
					indexToDragTo++;
				
				//We set the current owner so the item renderer doesnt get duplicated
				Main.currentOwner.dataProvider.addItemAt(dataBeingDragged, indexToDragTo);
				
				this._hiliteTop.visible = false;
				this._hiliteBottom.visible = false;
			}
			
			stopScrolling();
		}
		
		
	}
}