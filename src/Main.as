package
{
	import feathers.controls.List;
	import feathers.controls.ScrollContainer;
	import feathers.data.ListCollection;
	import feathers.events.FeathersEventType;
	import feathers.layout.TiledColumnsLayout;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;

	public class Main extends Sprite
	{

		public static var originalOwner:List;
		public static var currentOwner:List;
		

		public static var mainGroup:ScrollContainer;
		
		public function Main()
		{
			this.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}
		
		protected function addedToStageHandler(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			
			new CustomTheme();
			
			var myArray:Array = new Array();
			
			for(var i:uint = 0; i<100; i++){
				myArray.push({label:"Item Label "+i, sublabel:"Item Subtitile "+i, accessorylabel:"Third Label"});
			}
			
			
			var layoutForMainGroup:TiledColumnsLayout = new TiledColumnsLayout();
			layoutForMainGroup.requestedRowCount = 1;
			layoutForMainGroup.requestedColumnCount = 1;
			layoutForMainGroup.useSquareTiles = false;
			
			mainGroup = new ScrollContainer();
			mainGroup.backgroundSkin = new Quad(3, 3, 0xCC0000);
			mainGroup.layout = layoutForMainGroup;
			mainGroup.width = stage.stageWidth;
			mainGroup.height = stage.stageHeight;
			mainGroup.x = mainGroup.y = 0;
			mainGroup.hasElasticEdges = true;
			mainGroup.snapToPages = true;
			mainGroup.pageWidth = stage.stageWidth;
			mainGroup.pageHeight = stage.stageHeight;
			mainGroup.addEventListener("drag-started", function():void
			{
				trace("Disable ElasticEdges and PageSnapping");
				mainGroup.hasElasticEdges = false;
				mainGroup.snapToPages = false;
				
			});
			
			mainGroup.addEventListener("drag-stopped", function():void
			{
				trace("Disable ElasticEdges and PageSnapping");
				mainGroup.hasElasticEdges = true;
				mainGroup.snapToPages = true;
				
			});
			
			this.addChild(mainGroup);
			
			var list1:List = new List();
			
			list1.backgroundSkin = new Quad(3, 3, 0x000000);
			list1.name = "list1";
			list1.hasElasticEdges = false;
			list1.itemRendererType = DragableRenderer;
			list1.width = stage.stageWidth;
			list1.height = stage.stageHeight;
			list1.padding = 20;
			list1.dataProvider = new ListCollection(myArray);
			mainGroup.addChild(list1);
			
			
			var list2:List = new List();
			list2.backgroundSkin = new Quad(3, 3, 0x000000);
			list2.hasElasticEdges = false;
			list2.name = "list2";
			list2.itemRendererType = DragableRenderer;
			list2.width = stage.stageWidth;
			list2.height = stage.stageHeight;
			list2.padding = 20;
			list2.dataProvider = new ListCollection([{label:"Item 4"}, {label:"Item 5"}]);
			mainGroup.addChild(list2);
			
			
		}
		
	}
}